# Mastodon Timeline Reader

A simple to use, offline focused Mastodon Timeline viewer. Saves formatted data to cache for simple access. Nothing facy, one call

**Requirements**: This requires `jq` which I recomend having if you consume a
lot of data via shell scripts, it makes your life a lot easier.

### What do?

The main file is `./mastodon-update.sh`, this is only ran when you want to update the
data. Use an alias to access the data and grep what you need.
`cat` or `less` the cached formatted data and that's about it. 

### Alias

If you want to make full use of this script I'd recomend making an alias for it, it's a more straightforward approach. Since the data is within the `$HOME/.cache` directory then it makes sense to let the user decide how they want to go about it.

Below are some examples of how you could go about setting some aliases:

``` sh
alias mastodon="less ~/.cache/mastodon_timeline" \
      mastodonjson="less ~/.cache/mastodon_json" \
      mastupdate="~/Code/shell/mastodon/mastodon-update.sh" \
```

### Cron

If you're big-brain and not have to worry about running an alias everytime you want something, then think about modifying the script by removing the `case` and setting to a cron job. I've made it so you can do as you please with it.

**Note**: Remember to run the update script first, after that it's saved
in that `$HOME/.cache/mastodon_timeline` so just cat that to see what there is.


